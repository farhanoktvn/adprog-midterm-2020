package id.ac.ui.cs.advprog.midterm.controller;

import id.ac.ui.cs.advprog.midterm.entity.User;
import id.ac.ui.cs.advprog.midterm.entity.UserDTO;
import id.ac.ui.cs.advprog.midterm.repository.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;

import java.util.HashMap;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class UserControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UserRepository repository;

    @Mock
    private BindingResult result;

    @Mock
    private Model model;

    private User user;
    private UserDTO userDto;
    private String userId;
    private String INVALID_USER_ID = "Invalid user Id";

    @BeforeEach
    public void setupTest() {
        user = new User();
        user.setId(123456);
        user.setName("Marc");
        user.setEmail("123@aol.com");
        userId = Long.toString(user.getId());

        userDto = new UserDTO();
        userDto.setName("Marc");
        userDto.setEmail("123@aol.com");

        when(repository.findById(user.getId()))
                .thenReturn(java.util.Optional.ofNullable(user));
    }

    @Test
    public void whenSignUpURLIsAccessedItShouldShowCorrectTemplate() throws Exception {
        mockMvc.perform(get("/signup"))
                .andExpect(status().isOk())
                .andExpect(view().name("add-user"));
    }

    @Test
    public void whenEditURLIsAccessedItShouldShowCorrectTemplate() throws Exception {
        mockMvc.perform(get("/edit/" + userId))
                .andExpect(status().isOk())
                .andExpect(view().name("update-user"));
    }

    @Test
    public void whenEditURLIsAccessedButIdNotFoundRaiseException() throws Exception {
        try{
            mockMvc.perform(get("/edit/0"));
        } catch(Exception e) {
            assertThat(e).hasMessageContaining(INVALID_USER_ID);
        }
    }

    @Test
    public void whenDeleteURLIsAccessedItShouldShowCorrectTemplate() throws Exception {
        mockMvc.perform(get("/delete/" + userId))
                .andExpect(status().isOk())
                .andExpect(view().name("index"));
    }

    @Test
    public void whenDeleteURLIsAccessedButIdNotFoundRaiseException() throws Exception {
        try{
            mockMvc.perform(get("/delete/0"));
        } catch(Exception e) {
            assertThat(e).hasMessageContaining(INVALID_USER_ID);
        }
    }

    @Test
    public void whenAddURLIsAccessedItShouldShowCorrectTemplate() throws Exception {
        HashMap params = new HashMap();
        params.put("user", userDto);
        params.put("result", result);
        params.put("model", model);
        mockMvc.perform(post("/adduser")
                .flashAttrs(params))
                .andExpect(status().isOk())
                .andExpect(view().name("index"));
    }

    @Test
    public void whenAddURLIsAccessedButNotValidItShouldStay() throws Exception {
        HashMap params = new HashMap();
        params.put("user", new UserDTO());
        params.put("result", result);
        params.put("model", model);
        mockMvc.perform(post("/adduser")
                .flashAttrs(params))
                .andExpect(status().isOk())
                .andExpect(view().name("add-user"));
    }

    @Test
    public void whenUpdateURLIsAccessedItShouldShowCorrectTemplate() throws Exception {
        HashMap params = new HashMap();
        params.put("user", userDto);
        params.put("result", result);
        params.put("model", model);
        mockMvc.perform(post("/update/" + userId)
                .flashAttrs(params))
                .andExpect(status().isOk())
                .andExpect(view().name("index"));
    }

    @Test
    public void whenUpdateURLIsAccessedButNotValidItShouldStay() throws Exception {
        HashMap params = new HashMap();
        params.put("user", new UserDTO());
        params.put("result", result);
        params.put("model", model);
        mockMvc.perform(post("/update/" + userId)
                .flashAttrs(params))
                .andExpect(status().isOk())
                .andExpect(view().name("update-user"));
    }

    @Test
    public void whenUpdateURLIsAccessedButIdNotFoundRaiseException() throws Exception {
        HashMap params = new HashMap();
        params.put("user", new UserDTO());
        params.put("result", result);
        params.put("model", model);
        try {
            mockMvc.perform(post("/update/0")
                    .flashAttrs(params))
                    .andExpect(status().isOk())
                    .andExpect(view().name("update-user"));
        } catch(Exception e) {
            assertThat(e).hasMessageContaining(INVALID_USER_ID);
        }
    }

    @Test
    public void whenSearchURLIsAccessedItShouldShowCorrectTemplate() throws Exception {
        mockMvc.perform(get("/search/test"))
                .andExpect(status().isOk())
                .andExpect(view().name("index"));
    }

    @Test
    public void whenUpvoteURLIsAccessedItShouldShowCorrectTemplate() throws Exception {
        mockMvc.perform(get("/upvote/" + userId))
                .andExpect(status().isOk())
                .andExpect(view().name("index"));
    }

    @Test
    public void whenUpvoteURLIsAccessedButIdNotFoundRaiseException() throws Exception {
        try{
            mockMvc.perform(get("/upvote/0"));
        } catch(Exception e) {
            assertThat(e).hasMessageContaining(INVALID_USER_ID);
        }
    }

    @Test
    public void whenDownvoteURLIsAccessedItShouldShowCorrectTemplate() throws Exception {
        mockMvc.perform(get("/downvote/" + userId))
                .andExpect(status().isOk())
                .andExpect(view().name("index"));
    }

    @Test
    public void whenDownvoteURLIsAccessedButIdNotFoundRaiseException() throws Exception {
        try{
            mockMvc.perform(get("/downvote/0"));
        } catch(Exception e) {
            assertThat(e).hasMessageContaining(INVALID_USER_ID);
        }
    }
}
