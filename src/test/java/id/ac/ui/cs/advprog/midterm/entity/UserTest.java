package id.ac.ui.cs.advprog.midterm.entity;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.test.context.ActiveProfiles;

import static org.assertj.core.api.Assertions.assertThat;

@ActiveProfiles("test")
public class UserTest {

    private User user;

    @BeforeEach
    public void setUp() {
        user = new User();
        user.setName("James");
    }

    @Test
    public void testIdSetterAndGetter() {
        long id = 12345;
        user.setId(id);
        assertThat(user.getId()).isEqualTo(id);
    }

    @Test
    public void testNameSetterAndGetter() {
        String name = "Marc";
        user.setName(name);
        assertThat(user.getName()).isEqualTo(name);
    }

    @Test
    public void testEmailSetterAndGetter() {
        String email = "123@aol.com";
        user.setEmail(email);
        assertThat(user.getEmail()).isEqualTo(email);
    }

    @Test
    public void testVotesSetterAndGetter() {
        int votes = 5;
        user.setVotes(votes);
        assertThat(user.getVotes()).isEqualTo(votes);
    }
}
