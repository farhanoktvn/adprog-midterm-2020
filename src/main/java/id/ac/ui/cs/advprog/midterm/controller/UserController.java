package id.ac.ui.cs.advprog.midterm.controller;

import id.ac.ui.cs.advprog.midterm.entity.User;
import id.ac.ui.cs.advprog.midterm.entity.UserDTO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import id.ac.ui.cs.advprog.midterm.repository.UserRepository;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;
import java.util.List;

@Controller
public class UserController {

    private static final String INDEX = "index";
    private static final String USERS = "users";
    private String invalidUserId = "Invalid user Id: ";

    @Autowired
    UserRepository userRepository;

    @GetMapping("/signup")
    public String showSignUpForm(@ModelAttribute("user") UserDTO userDto) {
        return "add-user";
    }

    @PostMapping("/adduser")
    public String addUser(@ModelAttribute("user") @Valid UserDTO userDto, BindingResult result, Model model) {
        User user = new User();
        BeanUtils.copyProperties(userDto, user);
        if (result.hasErrors()) {
            return "add-user";
        }
        userRepository.save(user);
        model.addAttribute(USERS, userRepository.findAll());
        return INDEX;
    }

    @GetMapping("/edit/{id}")
    public String showUpdateForm(@PathVariable("id") long id, Model model) {
        User user = userRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException(invalidUserId + id));

        model.addAttribute("user", user);
        return "update-user";
    }

    @PostMapping("/update/{id}")
    public String updateUser(@PathVariable("id") long id, @ModelAttribute("user") @Valid UserDTO userDto,
                             BindingResult result, Model model) {
        User user = userRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException(invalidUserId + id));
        BeanUtils.copyProperties(userDto, user);
        if (result.hasErrors()) {
            user.setId(id);
            return "update-user";
        }

        userRepository.save(user);
        model.addAttribute(USERS, userRepository.findAll());
        return INDEX;
    }

    @GetMapping("/delete/{id}")
    public String deleteUser(@PathVariable("id") long id, Model model) {
        User user = userRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException(invalidUserId + id));
        userRepository.delete(user);
        model.addAttribute(USERS, userRepository.findAll());
        return INDEX;
    }

    @GetMapping("/search/{name}")
    public String deleteUser(@PathVariable("name") String name, Model model) {
        List<User> found = userRepository.findByNameContainingIgnoreCase(name);
        model.addAttribute(USERS, found);
        return INDEX;
    }

    @GetMapping("/upvote/{id}")
    public String upvoteUser(@PathVariable("id") long id, Model model) {
        User user = userRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException(invalidUserId + id));
        user.setVotes(user.getVotes() + 1);
        userRepository.save(user);
        model.addAttribute(USERS, userRepository.findAll());
        return INDEX;
    }

    @GetMapping("/downvote/{id}")
    public String downvoteUser(@PathVariable("id") long id, Model model) {
        User user = userRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException(invalidUserId + id));
        user.setVotes(user.getVotes() - 1);
        userRepository.save(user);
        model.addAttribute(USERS, userRepository.findAll());
        return INDEX;
    }
}